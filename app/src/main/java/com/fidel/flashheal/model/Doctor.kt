 package com.fidel.flashheal.model

data class Doctor(
    val doctor_id: String,
    val first_name: String,
    val last_name: String,
    val specialty: String,
    val latitude: Double,
    val longtitude: Double,
    val avg_review : Double,
    val reviews_count : Int
)