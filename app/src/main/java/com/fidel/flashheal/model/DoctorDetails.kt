package com.fidel.flashheal.model

data class DoctorDetails (
    val doctor_id: String,
    val star_rating: Int,
    val reviewer: String,
    val opinion: String
)