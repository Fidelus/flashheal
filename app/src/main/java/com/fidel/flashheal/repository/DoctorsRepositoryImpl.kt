package com.fidel.flashheal.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.fidel.flashheal.model.Doctor
import com.fidel.flashheal.model.DoctorDetails
import com.fidel.flashheal.network.DoctorsService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DoctorsRepositoryImpl() : DoctorsRepository {

    private val api: DoctorsService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://flashhealapi.azurewebsites.net/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        api = retrofit.create(DoctorsService::class.java)
    }

    override fun getDoctors(): LiveData<List<Doctor>> {
        val doctorsLiveData = MutableLiveData<List<Doctor>>()

        api.getDoctorsList().enqueue(object: Callback<List<Doctor>> {
            override fun onFailure(call: Call<List<Doctor>>, t: Throwable) {
                doctorsLiveData.value = emptyList()
            }

            override fun onResponse(call: Call<List<Doctor>>, response: Response<List<Doctor>>) {
                doctorsLiveData.value = response.body()
            }
        })
        return doctorsLiveData
    }

    override fun getDoctorDetails(doctor_id : String): LiveData<List<DoctorDetails>> {
        val doctorDetailsLiveData = MutableLiveData<List<DoctorDetails>>()

        api.getDoctorDetails(doctor_id).enqueue(object: Callback<List<DoctorDetails>> {
            override fun onFailure(call: Call<List<DoctorDetails>>, t: Throwable) {
                doctorDetailsLiveData.value = emptyList()
            }

            override fun onResponse(call: Call<List<DoctorDetails>>, response: Response<List<DoctorDetails>>) {
                doctorDetailsLiveData.value = response.body()
            }
        })
        return doctorDetailsLiveData
    }
}