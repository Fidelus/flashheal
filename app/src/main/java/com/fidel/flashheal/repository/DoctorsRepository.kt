package com.fidel.flashheal.repository

import androidx.lifecycle.LiveData
import com.fidel.flashheal.model.Doctor
import com.fidel.flashheal.model.DoctorDetails

interface DoctorsRepository {

    fun getDoctors(): LiveData<List<Doctor>>

    fun getDoctorDetails(doctor_id : String): LiveData<List<DoctorDetails>>

}