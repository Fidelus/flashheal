package com.fidel.flashheal.network

import com.fidel.flashheal.model.Doctor
import com.fidel.flashheal.model.DoctorDetails
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface DoctorsService {

    @GET("doctor-summaries")
    fun getDoctorsList(): Call<List<Doctor>>

    @GET("doctor-reviews/{doctor_id}")
    fun getDoctorDetails(@Path("doctor_id") doctor_id: String): Call<List<DoctorDetails>>
}