package com.fidel.flashheal.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.fidel.flashheal.R
import com.fidel.flashheal.databinding.FragmentDoctorDetailsBinding
import com.fidel.flashheal.ui.viewElements.DoctorReviewMarginDecoration
import com.fidel.flashheal.ui.viewElements.DoctorReviewsAdapter
import com.fidel.flashheal.ui.viewmodels.DoctorViewModel
import com.fidel.flashheal.ui.viewmodels.DoctorViewModelFactory
import kotlin.math.round


class DoctorDetailsFragment : Fragment() {
    private lateinit var binding: FragmentDoctorDetailsBinding
    private lateinit var viewModel: DoctorViewModel
    private lateinit var viewModelFactory: DoctorViewModelFactory
    private lateinit var adapter: DoctorReviewsAdapter

    val args: DoctorDetailsFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_doctor_details, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModelFactory = DoctorViewModelFactory(args.doctorId)
        viewModel = ViewModelProvider(this, viewModelFactory).get(DoctorViewModel::class.java)
        adapter = DoctorReviewsAdapter()

        binding.doctorReviewsList.layoutManager = LinearLayoutManager(context)
        binding.doctorReviewsList.adapter = adapter
        binding.doctorReviewsList.addItemDecoration(DoctorReviewMarginDecoration(
            resources.getDimension(R.dimen.margin_small).toInt()
        ))

        setupDoctorDetailsUI()
    }

    private fun setupDoctorDetailsUI(){
        viewModel.getDoctorWithId().observe(viewLifecycleOwner, Observer {
                doctor ->
            binding.avgRatingTextView.text = doctor.avg_review.toString()
            binding.reviewsCountTextView.text = "(" + doctor.reviews_count.toString() + ")"
            binding.nameTextView.text = doctor.first_name + " " +doctor.last_name
            binding.specialtyTextView.text = doctor.specialty
            binding.specialtyImageView.setImageDrawable(resources.getDrawable(getDoctorIcon(doctor.specialty)))
        })

        viewModel.getDoctorDetails().observe(viewLifecycleOwner, Observer {doctorDetailsList ->
            adapter.updateDoctorReviewsList(doctorDetailsList)
        })
    }

    fun getDoctorIcon(specialty : String): Int {
        var doctorIcon : Int
        var drawable_name = specialty.replace("\\s".toRegex(), "_").toLowerCase() + "32"
        doctorIcon = resources.getIdentifier(drawable_name, "drawable", requireActivity().packageName)
        if (doctorIcon == 0) {
            doctorIcon = resources.getIdentifier("default32", "drawable", requireActivity().packageName)
        }
        return doctorIcon
    }
}