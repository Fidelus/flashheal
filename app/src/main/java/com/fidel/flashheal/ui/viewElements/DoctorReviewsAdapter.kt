package com.fidel.flashheal.ui.viewElements

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fidel.flashheal.R
import com.fidel.flashheal.model.DoctorDetails
import kotlinx.android.synthetic.main.item_review.view.*

class DoctorReviewsAdapter() : RecyclerView.Adapter<DoctorReviewsAdapter.ViewHolder>() {

    private var doctorReviewsList: List<DoctorDetails> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_review, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.reviewerText.text = doctorReviewsList[position].reviewer
        holder.opinionText.text = doctorReviewsList[position].opinion
        holder.ratingStars.rating = doctorReviewsList[position].star_rating.toFloat()
        holder.initialLetter.text = doctorReviewsList[position].reviewer.get(0).toString()
    }

    override fun getItemCount(): Int = doctorReviewsList.size

    fun updateDoctorReviewsList(doctorReviewsList: List<DoctorDetails>){
        this.doctorReviewsList = doctorReviewsList
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val reviewerText = itemView.reviewerTextView
        val opinionText = itemView.opinionTextView
        val ratingStars = itemView.starRatingBar
        val initialLetter = itemView.icon_text
    }

}