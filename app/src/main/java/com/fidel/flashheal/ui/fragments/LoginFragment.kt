package com.fidel.flashheal.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.fidel.flashheal.R
import com.fidel.flashheal.databinding.FragmentDoctorDetailsBinding
import com.fidel.flashheal.ui.viewElements.DoctorReviewMarginDecoration
import com.fidel.flashheal.ui.viewElements.DoctorReviewsAdapter
import com.fidel.flashheal.ui.viewmodels.DoctorViewModel
import com.fidel.flashheal.ui.viewmodels.DoctorViewModelFactory


class LoginFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }
}