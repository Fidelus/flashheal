package com.fidel.flashheal.ui.viewElements

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.fidel.flashheal.R
import com.fidel.flashheal.ui.utils.Utils
import kotlinx.android.synthetic.main.item_specialty_spinner.view.*

class SpecialtiesSpinnerAdapter(context: Context, private val specialties: List<String>, private val layoutResource: Int = R.layout.item_specialty_spinner) : ArrayAdapter<String>(context, layoutResource, specialties) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView ?: LayoutInflater.from(context).inflate(layoutResource, parent, false)
        val icon = view.findViewById(R.id.spinner_icon) as ImageView
        icon.setImageResource(Utils().getDoctorIcon(context, specialties[position]))
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView ?: LayoutInflater.from(context).inflate(layoutResource, parent, false)
        val icon = view.findViewById(R.id.spinner_icon) as ImageView
        val specialty = view.findViewById(R.id.spinner_textView) as TextView

        specialty.text = specialties[position]
        icon.setImageResource(Utils().getDoctorIcon(context, specialties[position]))
        if (position == 0) specialty.setTextColor(Color.GRAY) else specialty.setTypeface(Typeface.DEFAULT_BOLD)

        return view
    }
}
