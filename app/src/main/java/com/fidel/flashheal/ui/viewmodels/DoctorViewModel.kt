package com.fidel.flashheal.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.fidel.flashheal.model.Doctor
import com.fidel.flashheal.model.DoctorDetails
import com.fidel.flashheal.repository.DoctorsRepository


class DoctorViewModel(repository: DoctorsRepository, doctorId : String) : ViewModel() {

    private val doctors: LiveData<List<Doctor>> = repository.getDoctors()
    private val doctorsRepository : DoctorsRepository = repository
    private val _doctorId : String = doctorId
    val doctorId : String
        get() = _doctorId


    fun getDoctors(): LiveData<List<Doctor>> = doctors

    fun getDoctorsBySpecialty(specialty: String): LiveData<List<Doctor>> = Transformations.map(doctors) { doctorsList ->
        if (specialty.toLowerCase() != "all")  {
            doctorsList.filter { doctor ->
                doctor.specialty.toLowerCase() == specialty.toLowerCase()
            }
        }
        else doctorsList
    }

    fun getDoctorWithId() : LiveData<Doctor> = Transformations.map(doctors) { doctorsList ->
        doctorsList.filter {
                doctor -> doctor.doctor_id == _doctorId
        }.first()
    }

    fun getDoctorDetails(): LiveData<List<DoctorDetails>> = doctorsRepository.getDoctorDetails(_doctorId)

    fun getSpecialties(): LiveData<List<String>> = Transformations.map(doctors) {
        it.map {it.specialty}.distinct()
    }
}