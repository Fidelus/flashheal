package com.fidel.flashheal.ui.fragments

import android.content.pm.PackageManager
import android.graphics.ColorFilter
import android.location.Location
import androidx.fragment.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.fidel.flashheal.R
import com.fidel.flashheal.databinding.FragmentDoctorDetailsBinding
import com.fidel.flashheal.databinding.FragmentMainMapsBinding
import com.fidel.flashheal.model.Doctor
import com.fidel.flashheal.ui.utils.Utils
import com.fidel.flashheal.ui.viewElements.SpecialtiesSpinnerAdapter
import com.fidel.flashheal.ui.viewmodels.DoctorViewModel
import com.fidel.flashheal.ui.viewmodels.DoctorViewModelFactory
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MainMapsFragment : Fragment(), OnMapReadyCallback {
    private lateinit var map: GoogleMap
    private lateinit var lastLocation: Location
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private lateinit var viewModel: DoctorViewModel
    private lateinit var viewModelFactory: DoctorViewModelFactory
    private lateinit var binding: FragmentMainMapsBinding
    private val utils = Utils()


    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_maps, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.mainMap) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        viewModelFactory = DoctorViewModelFactory()
        viewModel = ViewModelProvider(requireActivity(), viewModelFactory).get(DoctorViewModel::class.java)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        setUpNavBar()
        setUpSpecialtySpinner()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap?.let {
            map = googleMap
            map.uiSettings.isZoomControlsEnabled = true

            setUpMap()
            viewModel.getDoctors().observe(this, Observer { doctorList ->
                createMapMarkers(doctorList)
            })

            // Navigate to DoctorDetailsFragment
            map.setOnInfoWindowClickListener { marker ->
                val action = MainMapsFragmentDirections.mapToDetailsAction(marker.tag as String)
                Navigation.findNavController(requireView()).navigate(action)
            }
        }
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(requireContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        map.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener { location ->
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 13f))
            }
        }
    }

    private fun setUpNavBar(){
        binding.bottomNavigationView.setOnNavigationItemSelectedListener { it ->
            when(it.itemId)
            {
                R.id.ic_profile -> requireView().findNavController().navigate(R.id.mapToLoginAction)
            }
            true
        }
    }

    private fun filterIconsBySpecialty(specialty: String){
        viewModel.getDoctorsBySpecialty(specialty).observe(viewLifecycleOwner, Observer { doctorList ->
            createMapMarkers(doctorList)
        })
    }

    private fun createMapMarkers(doctorList : List<Doctor>){
        map.clear()
        doctorList.forEach { doctor: Doctor ->
            map.addMarker(MarkerOptions()
                .position(LatLng(doctor.latitude, doctor.longtitude))
                .title(doctor.first_name + " " + doctor.last_name)
                .icon(BitmapDescriptorFactory.fromResource(utils.getDoctorIcon(requireContext(), doctor.specialty)))
                .snippet(doctor.specialty))
                .tag = doctor.doctor_id
        }
    }

    private fun setUpSpecialtySpinner(){
        viewModel.getSpecialties().observe(viewLifecycleOwner, Observer { specialties ->
            val extendedSpecialties = specialties.toMutableList()
            extendedSpecialties.add(0, "All")
            val specAdapter = SpecialtiesSpinnerAdapter(requireContext(), extendedSpecialties)
            binding.specialtySpinner.adapter = specAdapter
        })
        binding.specialtySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                var specialty : String = p0?.selectedItem as String
                filterIconsBySpecialty(specialty)
            }
        }
    }
}
