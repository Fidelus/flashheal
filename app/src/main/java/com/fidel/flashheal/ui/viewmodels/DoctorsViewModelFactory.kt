package com.fidel.flashheal.ui.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.fidel.flashheal.repository.DoctorsRepositoryImpl
import java.lang.IllegalArgumentException

class DoctorViewModelFactory(val doctor_id : String = "0") : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(DoctorViewModel::class.java)) {
            return DoctorViewModel(
                repository = DoctorsRepositoryImpl(),
                doctorId = doctor_id
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel")
    }
}