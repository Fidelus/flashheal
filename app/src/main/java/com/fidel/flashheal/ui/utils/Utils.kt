package com.fidel.flashheal.ui.utils

import android.content.Context

class Utils {

    fun getDoctorIcon(context: Context, specialty : String): Int {
        var doctorIcon : Int
        var drawable_name = specialty.replace("\\s".toRegex(), "_").toLowerCase() + "32"
        doctorIcon = context.resources.getIdentifier(drawable_name, "drawable", context.packageName)
        if (doctorIcon == 0) {
            doctorIcon = context.resources.getIdentifier("default32", "drawable", context.packageName)
        }
        return doctorIcon
    }
}